"""
Usage:
Put this wherever you need in order to reach it.

from xmltojson import XMLToJSON
XMLtoJSON(putyourxmlpathorvariablehere).main()
"""

import os.path
import json

try:
    from lxml import etree as ET
except ImportError:
    import xml.etree.ElementTree as ET

class XMLtoJSON():
    def __init__(self, xmlpath):
        self.xmlpath = xmlpath

    def main(self):
        self._output(self._convert(self._getroot()))

    def _convert(self, e):
        et = e.tag
        d = {}

        for k, v in e.attrib.items():
            d[k] = v

        for se in e:
            v = self._convert(se)
            t = se.tag
            v = v[t]

            try:
                d[t].append(v)
            except KeyError:
                d[t] = v
            except AttributeError:
                d[t] = [d[t], v]

        return {et: d}

    def _getroot(self):
        tree = ET.parse(self.xmlpath)
        return tree.getroot()

    def _output(self, info):
        name = os.path.basename(self.xmlpath).split(".")[0]
        output = os.path.join(os.path.dirname(self.xmlpath), name + ".json")

        with open(output, 'w') as jfile:
            json.dump(info, jfile, indent=4)
